from config import db, ma

# Employee Class
class Employee(db.Model):
    id = db.Column(db.Integer, primary_key = True)
    empid = db.Column(db.String(5), unique=True)
    name = db.Column(db.String(30))
    phone = db.Column(db.String(10))
    salary = db.Column(db.Float)
    permits = db.relationship('Permit', backref ='owner')
    #__table_args__ = (db.UniqueConstraint('empid', name='unique_employee_id'),)

    # Constructor
    def __init__(self, empid, name, phone, salary):
        self.empid = empid.upper()
        self.name = name
        self.phone = phone
        self.salary = salary

    # Get all attributes
    def getDetails(self):
        return {'id': self.id,'empid': self.empid ,'name': self.name,'phone': self.phone, 'salary': self.salary, 'permits': self.permits}
    

#Employee Schema
class EmployeeSchema(ma.Schema):
    class Meta:
        fields = ('id', 'empid', 'name', 'phone', 'salary','permits')


# Permit Class
class Permit(db.Model):
    id = db.Column(db.Integer, primary_key = True)
    pno = db.Column(db.Integer, nullable = False, unique=True)
    regNr = db.Column(db.String(6), nullable = False, unique=True)
    owner_id = db.Column(db.Integer, db.ForeignKey('employee.id'))
    #__table_args__=(db.UniqueConstraint('pno','regNr', name='unique_car_permit'),)


    # Constructor
    def __init__(self, pno, regNr, owner_id):
        self.pno = pno
        self.regNr = regNr
        self.owner_id = owner_id

    # Get all attributes
    def getPermitDetails(self):
        return {'id': self.id, 'pno': self.pno, 'regNr': self.regNr, 'owner_id': self.owner_id}

#Permit Schema
class PermitSchema(ma.Schema):
    class Meta:
        fields = ('id', 'pno', 'regNr', 'owner_id')


emp_schema = EmployeeSchema()
employees_schema = EmployeeSchema(many=True)

permit_schema = PermitSchema()
permits_schema = PermitSchema(many=True)