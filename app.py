"""
Project: CarPark System for DES
Description: A python sever-side application to demonstrate modelling of CarPark System case study
Authors: Shaarang Tanpure, Dr. Kinh Nguyen
Date Modified: 20/10/2019

all api should be run as 
localhost:5000/<api>

"""
from __future__ import absolute_import
from flask import Flask, request, jsonify, render_template, redirect, render_template_string
from config import *
import os
import markdown
from models import db, Employee, Permit, employees_schema, permits_schema



# List of documentation pages
DOC_PAGES = [
    {'1': {'usecase' : 'Add Employee', 'url':'addemployee', 'prev':'models','prevusecase':'Models', 
    'next': 'addpermit', 'nextusecase': 'Add Permit', 'progress':20}},
    {'2':{'usecase' : 'Add Permit', 'url':'addpermit', 'prev':'addemployee','prevusecase':'Add Employee', 
    'next': 'retrieveemployee', 'nextusecase': 'Retreive Employee Details', 'progress':30}},
    {'3':{'usecase' : 'Retrieve Employee Details', 'url':'retrieveemployee', 'prev':'addpermit','prevusecase':'Add Permit', 
    'next': 'editemployeephone', 'nextusecase': 'Edit Employee Phone', 'progress':40}},
    {'4': {'usecase' : 'Edit Employee Phone', 'url':'editemployeephone', 'prev':'addpermit','prevusecase':'Add Permit',
     'next': 'deletepermit', 'nextusecase': 'Delete Permit', 'progress':50}},
    {'5':{'usecase' : 'Delete Permit', 'url':'deletepermit', 'prev':'editemployeephone','prevusecase':'Edit Employee Phone',
    'next': 'deleteemployee', 'nextusecase': 'Delete Employee', 'progress':60}},
    {'6':{'usecase' : 'Delete Employee', 'url':'deleteemployee', 'prev':'deletepermit','prevusecase':'Delete Permit',
    'next': 'usage', 'nextusecase': 'Installation and Usage', 'progress':70}}
]

# Read Documentation File
def read_md(filename):
    with open(basedir + '/static/assets/'+filename) as file_name:
        return markdown.markdown(file_name.read(), extensions=['fenced_code','codehilite', 'md_in_html'])

# get all employees
@app.route('/employees', methods=['GET', 'POST'])
def get_employees():
    # Query all employees
    all_employees = Employee.query.all()
    result = employees_schema.dump(all_employees)
    output = []
    # if there are any results
    if len(result) != 0:
        # for each employee in result
        for emp in result:
            permits=[]
            # get employee id, empid, name, phone and salary
            obj = {'id': emp['id'], 'empid': emp['empid'],'name': emp['name'], 'phone': emp['phone'], 'salary': emp['salary'],'permits': permits}
            #for each permit that the employee owns
            for permit in emp['permits']:
                #append to obj -> pno and registration no
                obj['permits'].append({'pno': permit.pno, 'regNr': permit.regNr})
            output.append(obj)

    #return jsonify({'employees': output})  #uncomment this line to see json output only
    return render_template('employees.html', employees=output, searchEmployee=False) #comment this line to view json output only

#add employee
@app.route('/addemployee', methods=['POST'])
def add_employee():
    # get request body being passed with POST request
    data = request.get_json(force=True)
    empid = data['empid']
    name = data['name']
    phone = data['phone']
    salary = data['salary']
    result = ''
    status = None
    try:
        # Create new Employee
        new_employee = Employee(empid, name, phone, salary)
        # Add employee to db
        db.session.add(new_employee)
        db.session.commit()
        result = 'Employee added with id : '+str(new_employee.id)
        status = 200
    except exc.IntegrityError as e:
        # Integrity error would be thrown when a constraint in the database is violated.
        #result = 'Employee cannot be added' '''
        error = e.orig.args
        result = error[0]
        status = 400
    #print(result)
    
    return jsonify({'result': result}),status

# access employee profile - This route can be used to also look up employee by specifying id in request.url
@app.route('/employee/<id>', methods=['GET'])
def get_employee_byid(id):
    employee = ''
    permit = []
    try:
        # Find an employee whose empid matches the id passed in request
        emp = Employee.query.filter(Employee.empid == id.upper())
        #print(emp.all())
        #print("\n\n")
        # Call employee classes getDetails method on the employee object retrieved
        employee = emp.all()[0].getDetails()
        # for each permit that the employee has
        for p in employee['permits']:
            # add pno and registration number to permit list
            permit.append({'pno':p.pno,'regNr':p.regNr})
        employee['permits'] = permit
    except Exception as e:
        # if employee doesnt exists, throw exception
        employee = 'Employee with id '+ str(id) + ' does not exist!'
    #return jsonify({'result': employee})
    return render_template('employeeprofile.html', employee=employee)


# get employee with id [Search function ]
# - Only used to perform search for html content [ DO NOT USE FOR JSON QUERYING, USE get_employee_byid INSTEAD]
# using this route to search and remove employee [pre-condition]
@app.route('/searchemployee', methods=['POST'])
def search_employee():
    employee = ''
    permit = []
    try:
        id = request.get_json(force=True)
        # Find an employee whose empid matches the id passed in request
        emp = Employee.query.filter(Employee.empid == id['employeeid'].upper())
        # Call employee classes getDetails method on the employee object retrieved
        employee = emp.all()[0].getDetails()
        #print(employee)
        # for each permit that the employee has
        for p in employee['permits']:
            # add pno and registration number to permit list
            permit.append({'pno':p.pno,'regNr':p.regNr})
        employee['permits'] = permit
        del employee['id']
    except Exception as e:
        # if employee doesnt exists, throw exception
        employee = 'Employee with id '+ str(id['employeeid'].upper()) + ' does not exist!'
    return jsonify({'result': employee})
    #return render_template('employeeprofile.html', employee=employee, searchEmployee=True)


# update phone number
@app.route('/updatephone/<id>', methods=['PUT'])
def update_employee_phone(id):
    # get data being passed in the request body
    data = request.get_json(force=True)
    empid = data['empid']
    new_phone = data['phone']
    updated_record = ''
    output = []
    status = None
    try:
        # find employee whose empid matches empid passed in the body
        employee = Employee.query.filter(Employee.empid == id.upper())
        employee = employee.all()[0]
        # if no employee found. Throw exception
        if(employee == None):
            raise Exception('Update failed. Employee with id '+ str(id) + ' does not exist!')
        # change employee phone to new phone
        employee.phone = new_phone
        db.session.commit()
        # get updated employee record
        #updated_record = Employee.query.filter(Employee.empid == id.upper())
        updated_record = employee.getDetails()
        permit = []
        for p in updated_record['permits']:   
            permit.append({'pno': p.pno, 'regNr': p.regNr})
        updated_record['permits'] = permit
        status = 200

    except Exception as e:
        updated_record = str(e)
        status = 400
    return jsonify({'result': updated_record}), status
    

# get all permits
@app.route('/permits', methods = ["GET"])
def getPermits():
    # Retrieve all permits
    all_permits = Permit.query.all()
    result = permits_schema.dump(all_permits)
    output = []
    if(len(result) > 0):
        for p in all_permits:
            obj = p.getPermitDetails()
            obj['owner_name'] = p.owner.name
            obj['empid'] = p.owner.empid
            output.append(obj)
    #return jsonify({'result': output}) uncomment this line if want to receive a json output
    return render_template('permits.html',permits=output, searchPermit=False) #comment this line if do not want HTML output

# get permit by pno
@app.route('/permit/<pno>', methods = ["GET"])
def getPermit(pno):
    permitDetails = ''
    try:
        permit = Permit.query.filter(Permit.pno == pno.upper())
        permitDetails = permit.all()[0].getPermitDetails()
        permitDetails['empid'] = permit.all()[0].owner.empid
        permitDetails['owner_name'] = permit.all()[0].owner.name
    except Exception as e:
        permitDetails = 'Permit with pno : '+str(pno.upper())+ ' does not exist!'
    #return jsonify({'result': permitDetails}) uncomment this line if want to receive a json output
    return render_template('permitsinfo.html',permit=permitDetails)   #comment this line if do not want HTML output


# get permit by pno [Search function ]
# - Only used to perform search for html content [ DO NOT USE FOR JSON QUERYING, USE getPermit INSTEAD]
# using this route to search and remove employee [pre-condition]
@app.route('/searchpermit', methods = ["POST"])
def permitSearch():
    permitDetails = ''
    pno = request.get_json(force=True)
    try:
        permit = Permit.query.filter(Permit.pno == pno['pno'].upper())
        permitDetails = permit.all()[0].getPermitDetails()
        permitDetails['empid'] = permit.all()[0].owner.empid
        permitDetails['owner_name'] = permit.all()[0].owner.name
    except Exception as e:
        permitDetails = 'Permit with pno : '+str(pno['pno'].upper())+ ' does not exist!'
    return jsonify({'result': permitDetails})


# add a permit
@app.route('/addpermit', methods=["POST"])
def addPermit():
    data = request.get_json(force=True)
    pno = data['pno']
    regNr = data['regNr']
    owner_emp_no = data['empid']
    result = ''
    status = None
    try:
        permit_owner = ''
        try:
            emp = Employee.query.filter(Employee.empid == owner_emp_no).first()
            permit_owner = emp
            #print(permit_owner)
        except Exception as e:
            raise Exception(e)
        if(permit_owner != None):
            if(len(permit_owner.permits) < 3):
                permit = Permit(pno, regNr,permit_owner.id)
                permit_owner.permits.append(permit)
                #print(permit_owner.permits[0])
                db.session.add(permit_owner)
                db.session.commit()
                result = 'Permit added with id : '+str(permit.id)
                status = 200
            else:
                raise Exception('Employee already has 2 permits')
        else:
            result = 'Employee with empid: '+str(owner_emp_no)+ ' does not exist!' 
    except exc.IntegrityError as e:
        error = e.orig.args
        result = error[0]
        status = 400 
    return jsonify({'result': result}),status


# delete an employee
@app.route('/employee/<id>',methods=['DELETE'])
def delete_employee(id):
    result = ''
    status = False
    try:
        employee = Employee.query.filter(Employee.empid == id.upper()).first()
        if(len(employee.permits) > 0):
            raise Exception('Cannot delete employee with permits. Delete permit for the employee first')
        if(employee == None):
            raise Exception('Employee with empid: '+ str(id) + ' not found')
        db.session.delete(employee)
        db.session.commit()
        result = 'Employee with empid: '+ str(employee.empid) + ' deleted'
    except Exception as e:
        result = str(e)
        status = True
    return jsonify({'result': result, 'status': status})

# delete permit
@app.route('/permit/<pno>', methods=['DELETE'])
def delete_permit(pno):
    result = ''
    status = False
    try:
        permit = Permit.query.filter(Permit.pno == pno.upper()).first()
        if(permit == None):
            raise Exception('Permit with pno: '+ str(pno) + ' not found')
        db.session.delete(permit)
        db.session.commit()
        result = 'Permit with pno: '+ str(permit.pno) + ' deleted'
    except Exception as e:
        result = str(e)
        status = True
    return jsonify({'result':result, 'status': status})


#documentation route
@app.route('/documentation', methods=['GET'])
def documentation():
    page_documentation = read_md('introduction.md')
    return render_template('documentation.html', MDContent = page_documentation, progress="5",
     link_next= '/models', next_page = 'Models')

#models route
@app.route('/models', methods=['GET'])
def models():
    page_documentation = read_md('models.md')
    return render_template('documentation.html', MDContent = page_documentation, progress="15",
    link_prev = '/documentation',link_next= '/usecases/addemployee', next_page = 'Use Case', prev_page='Case Study')


#usecase documentation route
@app.route('/usecases/<usecase>', methods=['GET'])
def usecases(usecase):
    file_name = 'usecases/'+usecase+'.md'
    doc_details = None
    for page in DOC_PAGES:
        for values in page:
            if page[values]['url'] == usecase:
                doc_details = page[values]
                if usecase == 'deleteemployee': # if its the last usecase documentation
                    doc_details['next'] = '/usage'
                else:
                    doc_details['next'] = doc_details['next']
                break

    page_documentation = read_md(file_name)
    return render_template('documentation.html', MDContent = page_documentation, progress=doc_details['progress'],
    link_prev = '/usecases/'+doc_details['prev'],link_next= doc_details['next'], 
    next_page = doc_details['nextusecase'], prev_page=doc_details['prevusecase'])


# installation and usage route
@app.route('/usage', methods=['GET'])
def usage():
    page_documentation = read_md('installation.md')
    return render_template('documentation.html', MDContent = page_documentation, link_prev='/usecases/deleteemployee', 
    link_next= '/employees', next_page = 'Start', prev_page='Delete Employee', progress="100")

@app.errorhandler(404)
def not_found(error = None):
    '''
    Handle 404 HTTP Error
    Generated when request made for a non existing route
    returns a response in json format with custom error message 

    :param error: Initialized to Null
    '''
    message = {
				'status':404,
				'message':'Not Found - '+ request.url,
				}
    resp = jsonify(message)
    resp.status_code = 404
    return render_template('404.html',err=message)

@app.route('/', methods=['GET'])
def homepage():
    return render_template('./cpsdetails.html')


if __name__ == "__main__":
    #create db if does not exist
    with app.app_context():
        db.create_all()
    app.run(host='0.0.0.0', debug=True)

