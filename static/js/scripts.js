(function($){
    $(function(){
  
      $('.sidenav').sidenav();
  
    }); // end of document ready
  })(jQuery); // end of jQuery name space


$("#empsearchbtn").click(function(){
  $.ajax({
    url: '/searchemployee',
    dataType: 'json',
    type: 'post',
    contentType: 'application/json',
    data: JSON.stringify({'employeeid': $("#employee-search").val().toUpperCase()}),
    processData: false,
    success: function (data, textStatus, jQxhr) {
      $('#emptable tbody').children('tr').remove();
      let emp_data = data['result'];
      if(emp_data['name'] != undefined){
        $('#emptable tbody').append("<tr id='searched'><td>" + "<label for='group1'><input name='group1' type='radio' checked /></label>" + "</td><td>"+ 
                                  "<a href="+"/employee/"+emp_data['empid']+">"+emp_data['empid']+"</a></td><td>" 
                                    + emp_data['name'] + "</td><td>" 
                                    + emp_data['phone'] + "</td><td>"+ 
                                    + emp_data['salary'] + "</td><td>"+
                                    + emp_data['permits'].length + "</td></tr>");
        $("#removeemployee").removeClass('disabled');
        $("#editemployee").removeClass('disabled');
        $('#addemployee').attr('disabled',true);
        // click function for table row
        $('#searched').click(function(){
          console.log('True');
          $('#searched').css('background-color', '#e0e0e0');
        })
      }
      else{
        $('#emptable tbody').append("<tr><td colspan='6'>"+emp_data+"</td></tr>"); 
      }
    },
    error: function (jqXhr, textStatus, errorThrown) {
      console.log(errorThrown);
    }
  });
});

$("#addEmpDetails").click(function(){
  console.log("adding");

  $.ajax({
    url:'/addemployee',
    datatype:'json',
    type:'post',
    contentType:'application/json',
    data:JSON.stringify({
      'empid':$('#emp_id').val(),
      'name':$('#first_name').val() + " " + $('#last_name').val(),
      'phone':$('#empphone').val(),
      'salary': parseFloat($('#empsalary').val()) 
    }),
    processData: false,
    success: function(data, textStatus, jqXhr){
      if(jqXhr.status == 200){
        M.toast({'html':data['result'], displayLength: 2000, completeCallback: function(){
          window.location.replace('/employees');
        }});
      }
      if(jqXhr.status == 400){
        M.toast({'html':data['result'], displayLength: 3000});
      } 
    }
  })
})

$("#removeemployee").click(function(){

  let empid = $("#searched  td a").text();
  let confirmation = confirm("Do you want to delete employee - "+ empid);
  if(confirmation == true){
    deleteEmployee(empid);
  }
  else{
    console.log('canceled operation');
  }
})
$("#ownersearch").click(function(){
  $.ajax({
    url: '/searchemployee',
    dataType: 'json',
    type: 'post',
    contentType: 'application/json',
    data: JSON.stringify({'employeeid': $("#ownerid").val().toUpperCase()}),
    processData: false,
    success: function (data, textStatus, jQxhr) {
     let owner_data = data['result'];
     $("#permitdetails").show();
     $("#ownername").val(owner_data['name']);
    },
    error: function (jqXhr, textStatus, errorThrown) {
      console.log(errorThrown);
    }
  });
});

function permitSearch(){
  $.ajax({
    url: '/searchpermit',
    dataType: 'json',
    type: 'post',
    contentType: 'application/json',
    data: JSON.stringify({'pno': $("#permit-search").val().toUpperCase()}),
    processData: false,
    success: function (data, textStatus, jQxhr) {
      $('#permitTable tbody').children('tr').remove();
      let permit_data = data['result'];
      if(permit_data['regNr'] != undefined){
        $('#permitTable tbody').append("<tr id='permitsearched'><td>" + "<label for='group1'><input name='group1' type='radio' checked /></label>" + "</td><td>"+ 
                                  "<a href="+"/permit/"+permit_data['pno']+">"+permit_data['pno']+"</a></td><td>" 
                                    + permit_data['regNr'] + "</td><td>" 
                                    + permit_data['empid'] + "</td><td>"
                                    + permit_data['owner_name'] + "</td></tr>");
        $("#removepermit").removeClass('disabled');
        $('#addpermit').attr('disabled',true);
        // click function for table row
        $('#permitsearched').click(function(){
          console.log('True');
          $('#permitsearched').css('background-color', '#e0e0e0');
        })
      }
      else{
        $('#permitTable tbody').append("<tr><td colspan='6'>"+permit_data+"</td></tr>"); 
      }
    },
    error: function (jqXhr, textStatus, errorThrown) {
      console.log(errorThrown);
    }
  });
}

$("#removepermit").click(function(){

  let pno = $("#permitsearched  td a").text();
  let confirmation = confirm("Do you want to delete permit - "+ pno);
  if(confirmation == true){
    deletePermit(pno);
  }
  else{
    console.log('canceled operation');
  }
})

function clearfields(){
 let owner_name = $("#ownername").val();
  $('#permit_form')[0].reset();
  $("#ownername").attr('placeholder',"" );
}

function clearEmpFields(){
  let employee_name = $("#eid").val();
  $('#edit_emp_form')[0].reset();
  $("#eid").attr('placeholder',"" );
  $("#employee_name").attr('placeholder',"" );
  $("#employee_number").attr('placeholder',"" );
  $("#employee_salary").attr('placeholder',"" );
}


$("#addPermitDetails").click(function(){
  $.ajax({
    url:'/addpermit',
    datatype:'json',
    type:'post',
    contentType:'application/json',
    data:JSON.stringify({
      'empid':$('#ownerid').val(),
      'pno':$('#permitno').val(),
      'regNr': $('#regnr').val() 
    }),
    processData: false,
    success: function(data, textStatus, jqXhr){
      if(jqXhr.status == 200){
        M.toast({'html':data['result'], displayLength: 2000, completeCallback: function(){
          window.location.replace('/permits');
        }});
      }
      if(jqXhr.status = 400){
        M.toast({'html':data['result'], displayLength: 3000});
      } 
    }
  })
})



function deletePermit(pno){
  $.ajax({
    url: '/permit/'+pno,
    dataType: 'json',
    type: 'delete',
    contentType: 'application/json',
    processData: false,
    success: function (data, textStatus, jQxhr) {
     console.log('deleted');
     M.toast({html: data['result'], displayLength: 3000, completeCallback: function(){
      if(data['status'] === false){
        window.location.replace('/permits');
      }
     }})
     
    },
    error: function (jqXhr, textStatus, errorThrown) {
      console.log(errorThrown);
    }
  });
}

function deleteEmployee(id){
  $.ajax({
    url: '/employee/'+id,
    dataType: 'json',
    type: 'delete',
    contentType: 'application/json',
    processData: false,
    success: function (data, textStatus, jQxhr) {
     console.log('deleted');
     M.toast({html: data['result'], displayLength: 3000})
     if(data['status'] === false){
       window.location.replace('/employees');
     }
    },
    error: function (jqXhr, textStatus, errorThrown) {
      console.log(errorThrown);
    }
  });
}


// search for an employee to edit details
$("#employee_search").click(function(){
  $.ajax({
    url: '/searchemployee',
    dataType: 'json',
    type: 'post',
    contentType: 'application/json',
    data: JSON.stringify({'employeeid': $("#eid").val().toUpperCase()}),
    processData: false,
    success: function (data, textStatus, jQxhr) {
     let owner_data = data['result'];
     $("#employeedetails").show();
     $("#employee_name").val(owner_data['name']);
     $("#employee_number").val(owner_data['phone']);
     $("#employee_salary").val(owner_data['salary']);
     
    },
    error: function (jqXhr, textStatus, errorThrown) {
      console.log(errorThrown);
    }
  });
});

// edit employee details
$("#edit_empDetails").click(function(){
  let id = $("#eid").val().toUpperCase()
  $.ajax({
    url: '/updatephone/'+id,
    dataType: 'json',
    type: 'put',
    contentType: 'application/json',
    data: JSON.stringify(
      {
        'empid': $("#eid").val().toUpperCase(),
        'phone':$("#employee_number").val()
    }),
    processData: false,
    success: function (data, textStatus, jqXhr) {
      if(jqXhr.status == 200){
        M.toast({'html':'Employee updated', displayLength: 2000, completeCallback: function(){
          window.location.replace('/employee/'+id);
        }});
      }
      if(jqXhr.status == 400){
        M.toast({'html':data['result'], displayLength: 3000});
      } 
    }
  });
});


$(document).ready(function(){
  $('.modal').modal();
  $('.collapsible').collapsible();
  $("#permitdetails").hide();
  $("#employeedetails").hide();
  $("#emptable td").click(function() {
    let empid = $(this).text();
    console.log(empid);
});
});
