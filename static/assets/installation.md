## Installation and Usage
<br>

> #### Installation

<br>

##### __Required Software__:

<br>

You'll need to install the following software on your machines if they are not already installed.

<br>

1. ######  __Python(Version > 3.6)__ [Installation](https://www.python.org/downloads/)

2. ###### (Optional) __Postman__ [Download](https://www.postman.com/) to test api


<br>

__After installing the software's listed above, open your terminal/command prompt and enter the following command `pip3 install pipenv`__

<br>

![pipenv-installation](http://homepage.cs.latrobe.edu.au/stanpure/cpsystem/images/pipenv-install)

<br>

 __pipenv__ is a python packaging tool that helps us solve the problem of managing dependencies in production environment. It takes advantage of _pip_ and _virtualenv_ to create a virtual environment and allows us to install dependencies on the client's end. More info can be found [here](https://realpython.com/pipenv-guide/).


Once the installation has finished, navigate to the directory (carparksystem).

The Directory structure is as follows

        
        CarParkSystem
        |__ __pycache__ (Compiled python files)
        |__ static
            |__ assets (contains documentation files)
            |__ css (css files)
            |__ js (javascript files)
        |__ template (contains html files to be rendered)
        |__ __init__.py (root file for package)
        |__ app.py (contains api-route code. Contains __main__ method)
        |__ carparkdb.sqlite (SQLite Database)
        |__ config.py (contains configuration details)
        |__ models.py (contains orm mapping code for employee and permits models)
        |__ Pipfile (contains list of dependencies for our application)

<br>

![dir-structure](http://homepage.cs.latrobe.edu.au/stanpure/cpsystem/images/dir-structure.png)

<br>


> #### Usage

<br>

__Navigate to your project directory and enter the following command `pipenv install`__

<br>

![pipenv-installation](http://homepage.cs.latrobe.edu.au/stanpure/cpsystem/images/env-setup.png)

<br>

__Once the dependencies have been installed, run the application by entering the following command `pipenv run python app.py`__

<br>

![run-app](http://homepage.cs.latrobe.edu.au/stanpure/cpsystem/images/run-app.png)

<br>

###### ___You can access the application on your local machine on [http://localhost:5000](http://localhost:5000) or to get started, click on the 'Start' button ___

<br>