### Use-Cases

<br>


#### 3. Retrieve the name, phone extension and permit details of an employee

<br>

> ##### ___Use case Description___

<br>


   __Trigger/Goal__: Given the ID of an employee, retrieve the employee's name and phone extension, 
and permit number of registration number for each permit that belongs to the employee

  __Actors__: Operator

  __Main Flow:__

     1. Operator enters the employee's ID.

     2. System validates that the employee exists.

     3. System retrieves and displays the following information about the employee:

        + Name

        + Phone extension

        + For each permit of the employee:

        - Permit number

        - car's registration number

  __Extensions__:
     2a. The employee does not exist:
    
        1. System notifies Operator, and terminates the use case.


<br>

> ##### ___Atomic Use-case___


<br>

> ##### ___Implementation___

<br>

```python

# access employee profile - This route can be used to also look up employee by specifying id in request.url
# Only allowed GET Http-method

@app.route('/employee/<id>', methods=['GET'])
def get_employee_byid(id):
    employee = ''
    permit = []
    try:
        # Find an employee whose empid matches the id passed in request
        emp = Employee.query.filter(Employee.empid == id.upper())
        
        # Call employee classes getDetails method on the employee object retrieved
        employee = emp.all()[0].getDetails()
        
        # for each permit that the employee has
        for p in employee['permits']:
            
            # add pno and registration number to permit list
            
            permit.append({'pno':p.pno,'regNr':p.regNr})
        
        employee['permits'] = permit
    
    except Exception as e:
        
        # if employee doesnt exists, throw exception
        
        employee = 'Employee with id '+ str(id) + ' does not exist!'
    
    return jsonify({'result': employee})
```
<br>

