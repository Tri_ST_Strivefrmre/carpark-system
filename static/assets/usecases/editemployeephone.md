### Use-Cases

<br>


>#### Use Case: Edit/Update Employee Phone Number

<br>

##### - ___Use case Description___

<br>

> #####  ___Atomic Use-case___

<br>


##### - ___Implementation___

<br>

```python

# update phone number of employee. Allows only for PUT Http-method

@app.route('/updatephone/<id>', methods=['PUT'])
def update_employee_phone(id):
    # get data being passed in the request body
    data = request.get_json(force=True)
    empid = data['empid']
    new_phone = data['phone']
    updated_record = ''
    output = []
    try:
        # find employee whose empid matches empid passed in the body
        employee = Employee.query.filter(Employee.empid == id.upper())
        employee = employee.all()[0]

        # if no employee found. Throw exception
        if(employee == None):
            raise Exception('Update failed. Employee with id '+ str(id) + ' does not exist!')
        
        # change employee phone to new phone
        employee.phone = new_phone
        db.session.commit()
        
        # get updated employee record
        #updated_record = Employee.query.filter(Employee.empid == id.upper())
        updated_record = employee.getDetails()
        permit = []
        for p in updated_record['permits']:   
            permit.append({'pno': p.pno, 'regNr': p.regNr})
        updated_record['permits'] = permit

    except Exception as e:
        updated_record = str(e)
    return jsonify({'result': updated_record})
```

<br>