### Use-Cases

<br>

#### 1. Add Employee

<br>

>#####  ___Use case Description___

<br>

__Trigger/Goal__: To enter the details of a new employee

__Actors__: Operator (or Car park manager)

__Main Flow:__
    
    1. Operator enters employee's ID.
    
    2. System validates that ID is new.
    
    3. Operator enters name and phone number.
    
    4. System saves the details.

__Extensions:__
    
    2a. ID is not new:
    
        1. System notifies Operator, and terminates the use case

<br>


> ##### ___Atomic Use-case___

<br>
 
   __in__:

        id? : String
        
        name?: String
        
        phone?: String
    
  __out__:
    
        NONE
 
   __pre__:
    
        // id? is new
        
        not exists e in employeeList | e.id = id?
 
   __post__:
    
        // add new employee
        
        let e = new Employee (id?, name?, and phone?)|
        
        e.id = id?
        
        e.name = name?
        
        e.phone = phone?
        
        e.permits = new Set<Permit>( )
        
        add e to employeeList

<br>

> ##### ___Implementation___

<br>

```python

@app.route('/addemployee', methods=['POST']) #define api route, Allows only POST Http-method

def add_employee():

    # get request body being passed with POST request

    data = request.get_json(force=True)

    empid = data['empid']

    name = data['name']

    phone = data['phone']

    salary = data['salary']

    result = ''

    status = None

    try:

        # Create new Employee

        new_employee = Employee(empid, name, phone, salary)

        # Add employee to db

        db.session.add(new_employee)

        db.session.commit() #commit changes

        result = 'Employee added with id : '+str(new_employee.id)

        status = 200

    except exc.IntegrityError as e:

        # Integrity error would be thrown when a constraint(like pk, fk violation) in the database is violated.

        #result = 'Employee cannot be added' '''

        error = e.orig.args

        result = error[0]

        status = 400

    print(result)
    
    return jsonify({'result': result}),status
```

<br>