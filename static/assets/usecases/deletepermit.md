### Use-Cases

<br>


>#### Use Case: Delete Permit of an Employee

<br>

##### - ___Use case Description___

<br>

> #####  ___Atomic Use-case___

<br>


##### - ___Implementation___

<br>

```python

# Given a permit number, delete permit of an employee. Allows only DELETE Http-method

@app.route('/permit/<pno>', methods=['DELETE'])
def delete_permit(pno):
    result = ''
    status = False
    try:
        # Find a permit record where permit number matches the permit no provided in request. First() retrieves first record encountered.
        permit = Permit.query.filter(Permit.pno == pno.upper()).first()
        if(permit == None):
            raise Exception('Permit with pno: '+ str(pno) + ' not found')
        db.session.delete(permit)
        db.session.commit()
        result = 'Permit with pno: '+ str(permit.pno) + ' deleted'
    except Exception as e:
        result = str(e)
        status = True
    return jsonify({'result':result, 'status': status})
```

<br>