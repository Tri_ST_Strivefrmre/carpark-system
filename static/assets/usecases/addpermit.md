### Use-Cases

<br>


#### 2. Add Permit

<br>

> #####  ___Use-case Description___

<br>

__Trigger/Goal__: To enter details of a new permit

__Actors__: Operator

__Main Flow__:
    
    1. Operator enters employee's ID.
    
    2. System validates that the employee with that ID exists and currently has less than 2
    permits.
    
    3. Operator enters the new permit number.
    
    4. System validates that the permit number is new.
    
    5. Operator enters the car's registration number.
    
    6. System validates that the registration number is new.
    
    7. System saves the new permit's details.

__Extensions:__
    
    2a. ID does not exist:
    
        1. System notifies Operator, and terminates the use case
    
    2b. Employee already has 2 permits:
    
        1. System notifies Operator, and terminates the use case
    
    4a. Permit number is not new:
    
        1. System notifies Operator, and terminates the use case
    
    6a. Registration number is not new:
    
        1. System notifies Operator, and terminates the use case

<br>

> #####  ___Atomic Use-case___

<br>
 
   __in__:

        permitNr?: String
        
        regNr?: String
        
        id? : String

    
  __out__:
    
        NONE
 
   __pre__:
    
        // permitNr? is new
        
        not exists p in permitList | p.permitNr = permitNr?

        // registration number is new
        
        not exists p in permitList | p.regNr = regNr?
        
        // id? exists and the employee has less than 2 permits
        
        exists e = element in employeeList |

        e.id = id?
        
        size (e.permits) < 2
 
   __post__:
    
        // retrieve the owner

        let owner = element in employeeList | e.id = id?

        // create a new permit

        let p = new Permit(permitNr?, regNr?, owner ) |

        p.permitNr = permitNr?

        p.regNr = regNr?

        p.owner = owner

        // update the set of permits of the owner

        add p to owner.permits

        // add new permit to the set of all permits

        add p to permitList

<br>

> ##### ___Implementation___

<br>

```python

@app.route('/addpermit', methods=["POST"]) #define api route, Allows only POST Http-method
def addPermit():
    data = request.get_json(force=True)
    pno = data['pno']
    regNr = data['regNr']
    owner_emp_no = data['empid']
    result = ''
    status = None
    try:
        permit_owner = ''
        try:
            # **Search Employee Record where empid matches the owners emp no provided by the operator**

            emp = Employee.query.filter(Employee.empid == owner_emp_no).first()
            permit_owner = emp
        except Exception as e:
            raise Exception(e)
        if(permit_owner != None):
            # Check if owner has no more than 0-2 permits

            if(len(permit_owner.permits) < 3):
                # Create a new Permit record
                permit = Permit(pno, regNr,permit_owner.id)
                # add new permit to list of permits of the owner

                permit_owner.permits.append(permit)
                db.session.add(permit_owner)
                db.session.commit()
                result = 'Permit added with id : '+str(permit.id)
                status = 200
            else:
                # If the employee already has 2 permits, throw an exception

                raise Exception('Employee already has 2 permits')
        else:
            result = 'Employee with empid: '+str(owner_emp_no)+ ' does not exist!' 
    except exc.IntegrityError as e:
        error = e.orig.args
        result = error[0]
        status = 400
    return jsonify({'result': result}),status

```

<br>
