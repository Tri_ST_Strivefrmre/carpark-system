### Use-Cases

<br>


>#### Use Case: Delete an Employee Record

<br>

##### - ___Use case Description___

<br>

> #####  ___Atomic Use-case___

<br>


##### - ___Implementation___

<br>

```python
# delete an employee record. Allows only for DELETE HTTP-method
@app.route('/employee/<id>',methods=['DELETE'])
def delete_employee(id):
    result = ''
    status = False
    try:

        #find an employee record where empid matches id provided by the operator

        employee = Employee.query.filter(Employee.empid == id.upper()).first()
        if(len(employee.permits) > 0):
            
            # check if the employee already has existing permits. If yes, operator should delete permit first.
            raise Exception('Cannot delete employee with permits. Delete permit for the employee first')
        if(employee == None):

            # if employee with id does not exist, throw employee not found exception
            raise Exception('Employee with empid: '+ str(id) + ' not found')
        db.session.delete(employee)
        db.session.commit()
        result = 'Employee with empid: '+ str(employee.empid) + ' deleted'
    except Exception as e:
        result = str(e)
        status = True
    return jsonify({'result': result, 'status': status})

```

<br >