> ### Models

<br>

**So based on our case-study, we have identified 3 classes that make our system -**

##### - Employee 
    
Attributes - ***_Employee Id, Full Name, Phone, Salary_***

##### - Permits
    
Attributes - ***_Permit No, Car Rego No_***

<br>

***These classes and their association(relationship) with each other will form our Car Park System.***

<br>





> ### Schema

<br>

###### Lets define our classes as a table in our database, we will use [Sqlite](https://www.sqlite.org/index.html) database and [Flask Sql-Alchemy](https://flask-sqlalchemy.palletsprojects.com/en/2.x/) which is a Flask extension for [Sql-Alchemy](https://www.sqlalchemy.org/).

<br>

###### ___SQLAlchemy is the Python SQL toolkit and Object Relational Mapper that gives application developers the full power and flexibility of SQL. SQLAlchemy provides a full suite of well known enterprise-level persistence patterns, designed for efficient and high-performing database access, adapted into a simple and Pythonic domain language.    SQLAlchemy is most famous for its object-relational mapper (ORM), an optional component that provides the data mapper pattern, where classes can be mapped to the database in open ended, multiple ways - allowing the object model and database schema to develop in a cleanly decoupled way from the beginning. More Info can be found [here](https://pypi.org/project/SQLAlchemy/).___ 

<br>
 ___
 
- ###### **Employee**

 Our Employee schema looks like this.

<br>

```python


    class Employee(db.Model):
        id = db.Column(db.Integer, primary_key = True)
        empid = db.Column(db.String(5), unique=True)
        name = db.Column(db.String(30))
        phone = db.Column(db.String(10))
        salary = db.Column(db.Float)
        permits = db.relationship('Permit', backref ='owner')

        # Constructor
        def __init__(self, empid, name, phone, salary):
            self.empid = empid.upper()
            self.name = name
            self.phone = phone
            self.salary = salary

        # Method to get all employee attributes
        def getDetails(self):
            return {'id': self.id,'empid': self.empid ,'name': self.name,'phone': self.phone, 'salary': self.salary, 'permits': self.permits}
```
<br>

###### Where:
**id** is auto-incremented when a new employee record is created

**empid** is the Employee id which is unique 

**name** is a `String` column for Employee name

**phone** is a `String` column for Employee phone

**salary** is a `Float` column in our **Employee** table

Since an __Employee__ can own more than `1` __Permit__, we need to represent this __One-to-Many__ relationship in our database.

We will create a __permit__ record in our __Employee__ table with a `backref` (link) to a Permit record for that Employee

*** 
- ###### **Permit**

<br>

 __Lets define our Permit Schema now__  

<br>

```python
class Permit(db.Model):
    id = db.Column(db.Integer, primary_key = True)
    pno = db.Column(db.Integer, nullable = False, unique=True)
    regNr = db.Column(db.String(6), nullable = False, unique=True)
    owner_id = db.Column(db.Integer, db.ForeignKey('employee.id'))
    

    # Constructor
    def __init__(self, pno, regNr, owner_id):
        self.pno = pno
        self.regNr = regNr
        self.owner_id = owner_id

    # Method to get all attributes of a permit
    def getPermitDetails(self):
        return {'id': self.id, 'pno': self.pno, 'regNr': self.regNr, 'owner_id': self.owner_id}

```
<br>

###### Where:
**id** is auto-incremented when a new permit record is created

**pno** is the Permit no, which is unique and can't be null

**regNr** is a `String` datatype for Car Registration number

**owner_id** is a `Integer`, which is a foreign key reference to the `id` column in Employee table.

Since a __Permit__ can only belong to `1` __Employee__, we have represented this relationship/association as __Foreign-key__ of the other table in our database.

<br>
