
> ### Problem Statement

<br>


1. A company requires a Car Park application to maintain information about employees
    and their parking permits.



2. Employees who want a permit have to pay a fee on a quarterly basis, which will be
    automatically deducted from their salary.

3. The purpose of the Car Park application is to help the car park manager to process the
    employees applications for parking permits

4. Each employee has an id, a name, and a phone extension.

5. An employees may change his/her phone extension during his/her employment period.


6. Each permit has a permit number and the cars registration number.


7. An employee can have at most two permits.


8. When an employee gets a new car and wants to use the new car instead of the old one,
    he/she has to discontinue the current permit and apply for a new one
    Further details:

9. Though the car park manager can access the employee's data from the Personnel Department, it is deemed convenient 
    for the car park application to maintain the required
    employee information for its own use.

10. When issuing or terminating a permit, the car park manager needs to inform Personnel
    Department of the change. However, this is done by a separate process which is regarded
    as outside the scope of this application.


<br>
<br>
<br>


