from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_marshmallow import Marshmallow
from flask_cors import CORS, cross_origin 
from sqlalchemy import exc
#from flaskext.markdown import Markdown
import os



app = Flask(__name__,template_folder='template')
CORS(app)

#setting up the base directory
basedir = os.path.abspath(os.path.dirname(__file__))

# Database config
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///' + os.path.join(basedir, 'carparkdb.sqlite')
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
app.config['SQLALCHEMY_ECHO'] = False # set to True if want to see changes in DB

# Initialize the db
db = SQLAlchemy()

db.init_app(app)

# Initialize marshmallow for serializing and de-serializing objects
ma = Marshmallow()