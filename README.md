## CAR PARK SYSTEM DEMONSTRATION FOR CSE2DES USING PYTHON-FLASK

### Steps to get started:

#### 1. pip install pipenv
#### 2. git clone repository
#### 3. cd to the repository and run - pipenv install to install dependencies from PipFile
#### 4. Run the application - pipenv run python app.py


#### For dependencies, check PipFile